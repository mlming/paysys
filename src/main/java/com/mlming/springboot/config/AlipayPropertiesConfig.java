package com.mlming.springboot.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 考虑到配置参数是经常需要改变的,结合之前开发经验我们知道,必须得把参数写到application.yml配置文件中去,然后动态获取
 * 所以我们使用@Configuration创建配置类对象
 * 同时通过@ConfigurationProperties注解直接获取到application.yml文件里面的自定义配置属性
 */
@Configuration
@ConfigurationProperties(prefix = "alipay")
@Data
public class AlipayPropertiesConfig {
    private String appId;

    private String privateKey;

    private String aliPayPublicKey;

    private String notifyUrl;

    private String returnUrl;

    private boolean sandbox;
}
