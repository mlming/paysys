package com.mlming.springboot.config;

import com.lly835.bestpay.config.AliPayConfig;
import com.lly835.bestpay.service.BestPayService;
import com.lly835.bestpay.service.impl.BestPayServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BestPayConfig {
    @Autowired
    private AlipayPropertiesConfig alipayPropertiesConfig; // 通过AlipayConfig配置类对象获取自定义属性
    /**
     * 在@Configuration修饰的配置类里面使用@Bean修饰的方法:
     * @Bean表示:
     *      该方法是一个返回一个对象的方法,随后该对象会交由Spring容器进行管理,其他地方可以获取该Bean对象
     *      该对象和其他的类上注解@Service,@Controller等一样,都是有且只有一次,在Spring启动时进行对象的创建与托管
     *      该对象就是该方法的返回值, 该对象的名称就是该方法的方法名
     * 此处其实就是在Spring初始化时,就创建一个BestPayService支付类对象,以供全局使用
     * 由于支付类对象的配置是不随订单信息为转移的,所以可以直接一开始就全局配置
     */
    @Bean
    public BestPayService bestPayService() {
        // 配置基本的支付信息, 并获取支付类对象
        AliPayConfig aliPayConfig = new AliPayConfig();
        // 通过配置类对象获取到配置参数
        aliPayConfig.setAppId(alipayPropertiesConfig.getAppId());
        aliPayConfig.setPrivateKey(alipayPropertiesConfig.getPrivateKey());
        aliPayConfig.setAliPayPublicKey(alipayPropertiesConfig.getAliPayPublicKey());
        aliPayConfig.setNotifyUrl(alipayPropertiesConfig.getNotifyUrl());// 异步通知,相当于访问127.0.0.1:8080/notify
        aliPayConfig.setReturnUrl(alipayPropertiesConfig.getReturnUrl()); // 异步通知也成功后,跳转的地址
        aliPayConfig.setSandbox(alipayPropertiesConfig.isSandbox()); // 设置为沙箱模式
        //支付类, 所有方法都在这个类里
        BestPayServiceImpl bestPayService = new BestPayServiceImpl();
        bestPayService.setAliPayConfig(aliPayConfig);

        return bestPayService;
    }
}
