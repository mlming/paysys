package com.mlming.springboot.controller;

import com.lly835.bestpay.enums.BestPayTypeEnum;
import com.lly835.bestpay.model.PayResponse;
import com.mlming.springboot.pojo.PayInfo;
import com.mlming.springboot.service.PayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;

@Controller
public class PayController {
    @Autowired
    private PayService payService;

    /**
     * 发起支付
     * @param orderNo
     * @param amount
     * @return
     */
    @RequestMapping("/create")
    public ModelAndView create(@RequestParam("orderid") Long orderNo,
                               @RequestParam("amount") BigDecimal amount,
                               @RequestParam("paytype") BestPayTypeEnum type) {
        PayResponse response = payService.createPay(orderNo, amount,type);
        ModelAndView mv = new ModelAndView();
        mv.addObject("body",response);
        mv.setViewName("pay");
        return mv;
    }

    /**
     * 支付宝异步通知配置的notify_url,会访问该方法
     * @return
     */
    @RequestMapping(value = "/notify",method = RequestMethod.POST)
    @ResponseBody
    public String asynNotify(@RequestBody String notifyData) {
        String result = payService.asynNotifyCheck(notifyData);
        return result;
    }
}
