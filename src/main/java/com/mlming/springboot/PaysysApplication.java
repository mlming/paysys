package com.mlming.springboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.mlming.springboot.dao")
public class PaysysApplication {

    public static void main(String[] args) {
        SpringApplication.run(PaysysApplication.class, args);
    }

}
