package com.mlming.springboot.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PayInfoVo {
    private Integer userId;

    private Long orderNo;

    private BigDecimal payAmount;

    private String platformStatus;
}
