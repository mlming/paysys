package com.mlming.springboot.enums;

import com.lly835.bestpay.enums.BestPayTypeEnum;

public enum PayPlatformEnum {
    ALIPAY(1),
    WX(2),
    ;
    Integer code;
    PayPlatformEnum(Integer code) {
        this.code = code;
    }

    /**
     * 检测支付平台合法性与返回该枚举值
     * @param type
     * @return
     */
    public static PayPlatformEnum getPayPlatformEnum(BestPayTypeEnum type) {
        for(PayPlatformEnum payPlatformEnum : PayPlatformEnum.values()) {
            if(type.getPlatform().name().equals(payPlatformEnum.name())) {
                return payPlatformEnum;
            }
        }
        throw new RuntimeException("错误的支付平台" + type);
    }

    /**
     * 获取code值
     * @return
     */
    public Integer getCode() {
        return this.code;
    }
}
