package com.mlming.springboot.service;

import com.lly835.bestpay.enums.BestPayTypeEnum;
import com.lly835.bestpay.model.PayResponse;

import java.math.BigDecimal;

public interface PayService {
    /**
     * 创建支付,发起支付
     * @param orderNo
     * @param payAmount
     * @return 第三方支付接口返回的支付信息(经过包的封装其实就是支付界面)
     */
    PayResponse createPay(Long orderNo, BigDecimal payAmount, BestPayTypeEnum type);

    /**
     * 检测支付的数据是否正确
     * @param notifyData
     * @return 状态
     */
    String asynNotifyCheck(String notifyData);
}
