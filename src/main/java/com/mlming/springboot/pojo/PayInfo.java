package com.mlming.springboot.pojo;

import com.mlming.springboot.enums.PayPlatformEnum;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class PayInfo {
    private Integer id;

    private Integer userId;

    private Long orderNo;

    private Integer payPlatform;

    private String platformNumber;

    private String platformStatus;

    private BigDecimal payAmount;

    private Date createTime;

    private Date updateTime;

    public PayInfo() {
    }
    // 生成支付信息的数据库数据时, 必须输入的其实就这三个,
    // 因为两个Date交由数据库自己生成,platformStatus支付状态与platformNumber支付流水号得等异步通知验证后
    public PayInfo(Long orderNo, BigDecimal payAmount, Integer payPlatformEnum) {
        this.orderNo = orderNo;
        this.payAmount = payAmount;
        this.payPlatform = payPlatformEnum;
    }
}