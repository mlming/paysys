# paysys支付系统

## 项目背景

* 随着在线支付的风潮,现在基本所有系统都要一套支付流程,所以我着手于开发这样一套通用型的支付系统,只提供对外接口,从而实现所有的系统都可以接入该支付系统,通过该支付系统去实现支付流程,实现了原系统的业务流程与支付流程的解耦合

## 相关数据库表: 支付信息表

![image-20211014184841419](F:\acer\ProjectForWork\paysys\doc\mdImage\image-20211014184841419.png)

## 技术栈:

* 微信 Native 支付接口 (由于需要资质,不采用)
* 支付宝 PC电脑网站 支付接口(虽然也需要资质,但是不上线的话可以使用官方提供的沙箱测试环境,所以本次采用)
* MyBatis 2.1.0
* MySQL 5.7
* SpringBoot 2.1.7
* RabbitMQ



## 支付理论:

* 微信支付开发文档: https://pay.weixin.qq.com/wiki/doc/api/index.html 

* 支付宝官方开发文档：https://docs.open.alipay.com/catalog

  

* 微信支付场景: 本项目使用: **Native支付(用户扫码支付)**

  ![img](F:\acer\ProjectForWork\paysys\doc\mdImage\1699B0F65E9B5364EA6D98996235C2B8.png)

* 支付宝支付场景:  本项目采用 **电脑网站支付**

  ![img](F:\acer\ProjectForWork\paysys\doc\mdImage\770EB8DAF87FCDB350EA0796C1A54FA3.png)

* 支付涉及到的两种id

![支付涉及的两种ID](F:\acer\ProjectForWork\paysys\doc\mdImage\支付涉及的两种ID.jpg)

## 支付宝接入准备:

* 跟着支付宝开发文档: https://opendocs.alipay.com/open/200/105310 一步步进行即可

* 密钥路径 : 按照官方文档的步骤下载开发助手生成本地文件: 例如: C:\Users\acer\Documents\支付宝开放平台开发助手\RSA密钥

* 四大密钥的作用:  **对于开发者而言,只知道商户的应用私钥,应用公钥,支付宝公钥**

  ![image-20211014205421574](F:\acer\ProjectForWork\paysys\doc\mdImage\image-20211014205421574.png)



## 支付业务流程:

* 通用型: 基本上后端与常用的第三方支付接口服务提供的支付功能都是如此交互的

  ![image-20211014213012558](F:\acer\ProjectForWork\paysys\doc\mdImage\image-20211014213012558.png)

* 结合支付宝特性 以及 结合实际实现代码型:

  ![image-20211014212057017](F:\acer\ProjectForWork\paysys\doc\mdImage\image-20211014212057017.png)



## 通过natapp.cn提供内网穿透技术:

* **获取到一个外网可以访问的ip地址映射到本地,使得不买服务器也可以实现上述notify_url** (当然,如果上线到购买的服务器,都有外网ip了就没必要用这种方式了)

* 获取方式: https://natapp.cn/  购买一个vip1,绑定二级域名

  ​				 **之后就可以通过生成的该地址去访问,然后会自动映射到访问127.0.0.1:8080 **

* 启动方式:  先在官网下载exe文件,然后在exe所在目录执行以下命令启动natapp服务,实现映射

  ```
  ./natapp -authtoken=上述获取后会有的一个authtoken
  ```



## 沙箱测试工具:

* 跟着开发文档即可 : https://opendocs.alipay.com/apis/01da4o

* 主要就是将代码中的支付类的配置参数都设置为沙箱提供的,同时设置为沙箱环境

  

## 项目初始化:

* pom.xml文件: 

  **使用了开源项目 best-pay-sdk 作为方便开发支付的包 **: https://github.com/Pay-Group/best-pay-sdk

```xml
<!-- web起步依赖 -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
<!-- SpringBoot集成thymeleaf的起步依赖 -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-thymeleaf</artifactId>
</dependency>
<!-- mysql依赖 -->
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
</dependency>
<!-- myBatis依赖 -->
<dependency>
    <groupId>org.mybatis.spring.boot</groupId>
    <artifactId>mybatis-spring-boot-starter</artifactId>
    <version>2.1.0</version>
</dependency>
<!-- lombok依赖 -->
<!-- 配合IDEA的lombok插件,可以通过@Data直接给实体类对象自动加上get,set,toString等方法,加快开发效率 -->
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
</dependency>

<!-- 开源支付项目依赖 https://github.com/Pay-Group/best-pay-sdk -->
<dependency>
    <groupId>cn.springboot</groupId>
    <artifactId>best-pay-sdk</artifactId>
    <version>1.3.0</version>
</dependency>
<!-- 解决使用@ConfigurationProperties注解出现警告的问题 -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-configuration-processor</artifactId>
    <optional>true</optional>
</dependency>
<!-- 测试依赖 -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-test</artifactId>
    <scope>test</scope>
</dependency>
<!-- RabbitMQ依赖 -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-amqp</artifactId>
</dependency>
```

* application.yml文件:

```yml
# 配置连接mysql数据库信息
spring:
  datasource:
    driver-class-name: 
    username: 
    password: 
    url: 
#关闭thymeleaf缓存
  thymeleaf:
    cache: false
# rabbitMQ 的相关配置
  rabbitmq:
    addresses: 192.168.131.133
    port: 5672
    username: admin
    password: admin
    virtual-host: /sapmall    

# 配置mapper文件所在位置s
mybatis:
  mapper-locations: classpath:mappers/*.xml
```



## 代码实现:

* 项目架构图:

  ![image-20211109170337980](F:\acer\ProjectForWork\paysys\doc\mdImage\image-20211109170337980.png)

  

* ***接下来的代码实现按照1.6支付流程逐步讲解***:

  * 第0步: 通过mybatis-generator生成dao层与mapper文件

    PayInfo:

    ```java
    @Data
    public class PayInfo {
        private Integer id;
    
        private Integer userId;
    
        private Long orderNo;
    
        private Integer payPlatform;
    
        private String platformNumber;
    
        private String platformStatus;
    
        private BigDecimal payAmount;
    
        private Date createTime;
    
        private Date updateTime;
    
        public PayInfo() {
        }
    	// 生成支付信息的数据库数据时, 必须输入的其实就这三个,
        // 因为两个Date交由数据库自己生成,platformStatus支付状态与platformNumber支付流水号得等异步通知验证后
        public PayInfo(Long orderNo, BigDecimal payAmount, Integer payPlatformEnum) {
            this.orderNo = orderNo;
            this.payAmount = payAmount;
            this.payPlatform = payPlatformEnum;
        }
    }
    ```

  * 第一步: 接收其他系统对本系统的支付请求: 

    PayController: 

    ```java
    @RequestMapping("/create")
    public ModelAndView create(@RequestParam("orderid") Long orderNo,
                                @RequestParam("amount") BigDecimal amount,
                                @RequestParam("paytype") BestPayTypeEnum type) {
         PayResponse response = payService.createPay(orderNo, amount,type);
         ModelAndView mv = new ModelAndView();
         mv.addObject("body",response);
         mv.setViewName("pay");
         return mv;
    }
    ```

  * 第二步: 支付宝支付接口的参数配置 以及 支付类的获取

    * 支付宝接口的参数配置:

      采用的是: application.yml配置自定义参数,然后通过@Configuration注解生成的配置类 加以 @ConfigurationProperties 自动注入该配置类的属性中

      application.yml: 

      ```xml
      alipay:
        appId: 
        privateKey: 
        aliPayPublicKey: 
        notifyUrl: "http://mlmingpay.natapp1.cc/notify"
        returnUrl: "http://127.0.0.1:8080"
        sandbox: true
      ```

      AlipayPropertiesConfig:

      ```java
      /**
       * 考虑到配置参数是经常需要改变的,结合之前开发经验我们知道,必须得把参数写到application.yml配置文件中去,然后动态获取
       * 所以我们使用@Configuration创建配置类对象
       * 同时通过@ConfigurationProperties注解直接获取到application.yml文件里面的自定义配置属性
       */
      @Configuration
      @ConfigurationProperties(prefix = "alipay")
      @Data
      public class AlipayPropertiesConfig {
          private String appId;
      
          private String privateKey;
      
          private String aliPayPublicKey;
      
          private String notifyUrl;
      
          private String returnUrl;
      
          private boolean sandbox;
      }
      ```

    * 支付类的获取:

      采用的@Configuration生成的配置类 加以 @Bean注解的方法进行进行支付类对象的创建,交由Spring容器管理

      BestPayConfig:

      ```java
      @Configuration
      public class BestPayConfig {
          @Autowired
          private AlipayPropertiesConfig alipayPropertiesConfig; // 通过AlipayPropertiesConfig配置类对象获取自定义属性
          
          /**
           * 在@Configuration修饰的配置类里面使用@Bean修饰的方法:
           * @Bean表示:
           *      该方法是一个返回一个对象的方法,随后该对象会交由Spring容器进行管理,其他地方可以获取该Bean对象
           *      该对象和其他的类上注解@Service,@Controller等一样,都是有且只有一次,在Spring启动时进行对象的创建与托管
           *      该对象就是该方法的返回值, 该对象的名称就是该方法的方法名
           * 此处其实就是在Spring初始化时,就创建一个BestPayService支付类对象,以供全局使用
           * 由于支付类对象的配置是不随订单信息为转移的,所以可以直接一开始就全局配置
           */
          @Bean
          public BestPayService bestPayService() {
              // 配置基本的支付信息, 并获取支付类对象
              AliPayConfig aliPayConfig = new AliPayConfig();
              // 通过配置类对象获取到配置参数
              aliPayConfig.setAppId(alipayPropertiesConfig.getAppId());
              aliPayConfig.setPrivateKey(alipayPropertiesConfig.getPrivateKey());
              aliPayConfig.setAliPayPublicKey(alipayPropertiesConfig.getAliPayPublicKey());
              aliPayConfig.setNotifyUrl(alipayPropertiesConfig.getNotifyUrl());// 异步通知,相当于访问127.0.0.1:8080/notify
              aliPayConfig.setReturnUrl(alipayPropertiesConfig.getReturnUrl()); // 异步通知也成功后,跳转的地址
              aliPayConfig.setSandbox(alipayPropertiesConfig.isSandbox()); // 设置为沙箱模式
              //支付类, 所有方法都在这个类里
              BestPayServiceImpl bestPayService = new BestPayServiceImpl();
              bestPayService.setAliPayConfig(aliPayConfig);
      
              return bestPayService;
          }
      }
      ```

  * 第三步: 通过支付类发起支付  同时会 获取到支付宝支付接口返回的信息(核心是body属性,是一个form表单html元素 => 渲染到前端->支付页面)

    PayService:

    ```java
    public interface PayService {
        /**
         * 创建支付,发起支付
         * @param orderNo
         * @param payAmount
         * @return 第三方支付接口返回的支付信息(经过包的封装其实就是支付界面)
         */
        PayResponse createPay(Long orderNo, BigDecimal payAmount, BestPayTypeEnum type);
    
        /**
         * 检测支付的数据是否正确
         * @param notifyData
         * @return 状态
         */
        String asynNotifyCheck(String notifyData);
    }
    ```

    PayServiceImpl:

    ```java
    @Service
    public class PayServiceImpl implements PayService {
        @Autowired
        private PayInfoMapper payInfoMapper;
        @Autowired
        private BestPayService bestPayService; // 由BestPayConfig配置类的@Bean方法配置生成的支付类
    
        @Override
        public PayResponse createPay(Long orderNo, BigDecimal payAmount,BestPayTypeEnum type) {
            // 将订单信息写入数据库
            payInfoMapper.insertSelective(new PayInfo(orderNo,payAmount,PayPlatformEnum.getPayPlatformEnum(type).getCode()));
            // 创建本次支付请求类对象,传入支付类的pay方法发起支付
            PayRequest payRequest = new PayRequest();
            payRequest.setOrderName("支付订单号");
            payRequest.setPayTypeEnum(type);
            payRequest.setOrderAmount(payAmount.doubleValue());
            payRequest.setOrderId(String.valueOf(orderNo));
            PayResponse response = bestPayService.pay(payRequest);
    
            return response;
        }
        
        @Override
        public String asynNotifyCheck(String notifyData) {} // 之后详解
    }
    ```

    前面提过的PayController调用PayServiceImpl的createPay方法时,会获得到PayResponse,要把其中的body属性渲染到前端页面上

    PayController:

    ```java
    PayResponse response = payService.createPay(orderNo, amount,type);
    ModelAndView mv = new ModelAndView();
    mv.addObject("body",response);
    mv.setViewName("pay");
    return mv;
    ```

    使用thymeleaf模板引擎的html界面:

    pay.html:

    ```html
    <!DOCTYPE html>
    <html lang="en" xmlns:th="http://www.thymeleaf.org">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>
    </head>
    <body>
        <div th:utext="${body.body}"></div>
    </body>
    </html>
    ```

  * 第四步: 用户扫码(和项目无关)

  * 第五步: 支付宝向配置的notify_url发送异步通知,通过Controller接收,进行**验证**,验证无误后,controller返回一个"success"字符串,通知支付宝不用发通知了

    PayController:

    ```java
    /**
    * 支付宝异步通知配置的notify_url,会访问该方法
    * @return
    */
    @RequestMapping(value = "/notify",method = RequestMethod.POST)
    @ResponseBody
    public String asynNotify(@RequestBody String notifyData) {
        String result = payService.asynNotifyCheck(notifyData);
        return result;
    }
    ```

    payServiceImpl:

    ```java
    @Override
    public String asynNotifyCheck(String notifyData) {
        // 1. 签名验证: 直接调用sdk提供的方法进行验证:
        // 如果签名验证错误,会直接抛出异常;如果验证无误,结果会返回一个经过把返回的String封装后的对象以供我们更好地获取异步通知的数据
        // PayResponse(prePayParams=null, payUri=null, appId=null, timeStamp=null, nonceStr=null, packAge=null, signType=null, paySign=null, orderAmount=1.0, orderId=123456, outTradeNo=2021101522001430600501566851, mwebUrl=null, body=null, codeUrl=null, attach=null, payPlatformEnum=ALIPAY)
        PayResponse notifyResponse = bestPayService.asyncNotify(notifyData);
        // 2. 金额验证: 从数据库取出之前存进去的支付信息,对金额进行验证
        PayInfo payInfo = payInfoMapper.selectByOrderNo(Long.parseLong(notifyResponse.getOrderId()));
        if(payInfo == null) {
        	throw new RuntimeException("数据库无该支付数据");
        }
        if(!(payInfo.getPayAmount().compareTo(BigDecimal.valueOf(notifyResponse.getOrderAmount())) == 0)) {
        // 如果金额验证不匹配:
        	throw new RuntimeException("数据库中的支付金额与异步通知的金额不匹配");
        }
        // 如果 签名验证与金额验证都通过了,则:
        // 3. 修改数据库中该支付信息的支付状态 以及 支付流水号
        payInfo.setPlatformStatus(OrderStatusEnum.SUCCESS.name());
        payInfo.setPlatformNumber(notifyResponse.getOutTradeNo());
        payInfoMapper.updateByPrimaryKeySelective(payInfo);
        // TODO: 4. 通过rabbitmq异步通知申请支付的系统, 把部分支付数据(新建一个vo类)发送回去
        // 5. 返回支付宝 通知成功的对应的"success"字符串
        return "success";
    }
  ```
  
* 第六步: 通过异步通知通知其他系统支付业务完成
  
    新建一个vo类,用于异步通知发送部分的支付信息
  
  ```java
    @Data
  public class PayInfoVo {
        private Integer userId;
    
        private Long orderNo;
    
        private BigDecimal payAmount;
    
        private String platformStatus;
    }
    ```
  
    ```java
    // 4. 通过rabbitmq异步通知申请支付的系统, 把部分支付数据发送回去
    PayInfoVo payInfoVo = new PayInfoVo();
    BeanUtils.copyProperties(payInfo,payInfoVo);
    rabbitTemplate.convertAndSend(RABBITMQ_QUEUE_NAME,new Gson().toJson(payInfoVo));
    ```
  
  * 第七步: 支付宝自动跳转到配置的return_url上
  
  * 支付业务流程结束



## 其他系统如何向该支付系统发起支付业务

* 首先我们知道我们的发起支付的controller方法对应的url是 ip地址:8080/create
* 那如果其他系统是通过ajax或其他发起请求的方式向该url发起请求时, 其实获得得只会是一个pay.html的源代码
* 所以, 其他系统要采用的是: **跳转到该url,从而由浏览器发起请求获取到pay.html的源代码,从而展示前端页面**
* 这也就是为什么我们controller方法的method是GET的原因



## 拓展: **可能出现的问题以及解决方案**

* 为什么不会重复发起支付呢?

  * 当用户支付完一次后,由于网络延迟, 订单没刷新, 所以用户又紧接着又扫码支付一次,会出现重复支付的情况吗?

    或者用户支付完后,由于网络延迟,再次通过原订单发起支付,会出现重复支付的情况吗?

  * 答案: 不会

  * 原因: 

    ​	支付平台一般也会对发过来的订单号,appid等进行一个认证,如果该订单的状态是已经支付的, 支付平台就不会再接收该订单的支付

    ​	或者扫码也不会有效

    

* 由于支付信息表的订单号使用了Unique唯一约束, 那么如果出现以下情况呢:

  * 问题: 

     **由于网络延迟,本次订单存入了数据库,但是却没有发送给支付平台进行支付或者发给了但是失败了**

     由于我们一次订单的订单号在其他系统那边必然是不变的,但是此时我们已经写入了支付信息表,且订单号有唯一约束

     那么,当其他系统再一次以该订单号发起支付请求时, 就会在写入数据库时失败,便无法完成本订单的支付功能了

  * 解决思路:

    ​	**每次当其他系统的订单支付请求过来时,我们都想办法生成一个唯一的映射字符串,然后将映射字符串作为该订单的订单号写入支付信息表中**

  * 解决的原因:

    ​	之前无法再次发起支付的原因,只是因为订单号在其他系统是确定的,因此发送给支付系统时,写入数据库会出现唯一冲突

    ​	那么,每次我们都生成的是一个唯一的映射字符串,然后写入数据库,不就能解决该问题了吗,反正最后被支付的都是原订单

  * 新问题的导致:

    ​	由于我们每次都是生成一个唯一的映射字符串写入数据库然后发起支付,那么就使得每次向支付平台发起的订单都是不一样的

    ​	那么上面提到的支付平台自带的对已支付的订单的重复支付的解决方案就不能生效了

    ​	**所以就有可能会导致重复支付的情况**

    ​	例如用户连续快速点击支付,那么就会在后台可能同一个订单生成了多个映射订单,然后同时发起了多个可以支付的支付订单

  * 新问题的解决方案: **同时也是原问题的最终最优解**

    - 第一步: 我们需要新建一张表, 用于存放: 映射字符串 与 原订单号 的映射关系 (下面称为映射表)

     - 第二步: 当一个新的订单号发过来时, 我们通过原订单号去映射表里面查出是否已经有过映射字符串

       ​			 如果有映射字符串,说明该订单必然已经发起过支付了,获取到**该映射字符串**,到**支付信息表**里查询该订单

       ​			- **如果该订单状态为success,说明已经支付成功过了, 则直接返回**

       ​			- **如果该订单状态不是success,则通过支付平台提供的 关闭订单的api 来先把之前的订单关闭掉**

    * 第三步: 当 1) 第二步没有映射字符串 或者 2) 有映射字符串但是已经把之前的订单关闭删除掉了,

      ​             则生成一个唯一的映射字符串,将映射字符串与该订单号的映射关系写入映射表,将订单的订单号改成映射字符串存入支付信息表中

      ​		    把该订单发起支付

  * 解决的原因:

    ​    通过映射表保存的映射关系,我们就可以通过原订单号查询到之前出现过的映射字符串,从而查询到之前出现过的支付信息

    ​	**如果success,则放弃支付;如果不是success,则先关闭之前的,再按原来的步骤发起支付**

    ​	这样就能确保了原订单号只被支付一次,解决了重复支付的问题



